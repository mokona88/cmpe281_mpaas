CREATE TABLE EC2Instance
(
	instanceId			SERIAL PRIMARY KEY,
    imageId				varchar(20),
    public_ip			varchar(20),
    public_ip_int 		BIGINT UNSIGNED,
    private_ip  		varchar(20),
    private_ip_int 		BIGINT UNSIGNED,
    public_dns  		varchar(100),
    private_dns 		varchar(100),
    platform			varchar(20),/* Value is Windows if it's Windows; otherwise, value correspond to imageId */
    AMILaunchIdx		varchar(20),
    architecture		varchar(10),
    virtType			varchar(20),
    stateReason			varchar(20),
    tag					varchar(200),
    rootDeviceName		varchar(100),
    rootDeviceType		varchar(100),
    launchTime			Date,
    keyname				varchar(100),
    instanceLifeCycle	varchar(150),
    hypervisor			varchar(100),
    instanceState		INT UNSIGNED,
    instanceStateName	varchar(30)
    /*
    The low byte represents the state. The high byte is an opaque internal value and should be ignored.
		0 : pending
		16 : running
		32 : shutting-down
		48 : terminated
		64 : stopping
		80 : stopped
		Parameters:
		code - The low byte represents the state. The high byte is an opaque internal value and should be ignored.
		0 : pending
		16 : running
		32 : shutting-down
		48 : terminated
		64 : stopping
		80 : stopped
    */
);

CREATE TABLE MobileApp
(
	AppId			SERIAL PRIMARY KEY,
    AppName			VARCHAR(100),
    platform		VARCHAR(20) NOT NULL,
    DeviceInfo		VARCHAR(50),
    SDKVersion		VARCHAR(5) NOT NULL
);

CREATE TABLE TestCase
(
	testCaseId		SERIAL,
    userId			BIGINT UNSIGNED NOT NULL,
    instanceId		BIGINT UNSIGNED NOT NULL,
    AppId			BIGINT UNSIGNED NOT NULL,
    startTime		DATETIME,
    endTime			DATETIME,
    targetSDK		int,
    platform		varchar(20),
    FOREIGN KEY (userId) REFERENCES users(id),
    FOREIGN KEY (instanceId) REFERENCES EC2Instance(instanceId),
    FOREIGN KEY (AppId) REFERENCES MobileApp(AppId)
);


SELECT INET_ATON('cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com');

show columns from users;