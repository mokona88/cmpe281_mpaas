use cmpe281;
create table users(
	user_id		int not null auto_increment,
	email 		varchar(255) NOT NULL,
	password    TEXT NOT NULL,
    first_name 	TEXT NOT NULL, 
    last_name 	TEXT NOT NULL,
    primary key (user_id),
    unique key  email (email)
);

insert into users values (1,'cindy@sjsu.edu','cindy','Cindy', 'Lee');

select * from users;
select * from TestCase;
select * from EC2Instance;
select * from MobileApp;
delete from TestCase; 

delete from TestCase where userId=1;
insert into TestCase values(1,16,8,null,'2014-04-21 16:27:30','2014-04-22 17:27:30')

select first_name, timestampdiff(minute,startTime,endTime) as diff From TestCase t1,users u1
where t1.userId=u1.id;

alter table EC2Instance ADD instanceName text NOT NULL

select * from EC2Instance;
select * from users;

Select distinct userId, instanceName from EC2Instance e1, TestCase t1 where e1.instanceId=8 AND t1.userId=16;


