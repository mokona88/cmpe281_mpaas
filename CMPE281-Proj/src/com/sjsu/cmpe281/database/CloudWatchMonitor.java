package com.sjsu.cmpe281.database;

import java.util.Date;
import java.util.List;
import java.util.Map; // Superclass of TreeMap
import java.util.TreeMap;
import java.util.Iterator; // Used to traverse the TreeMap

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsResult;
import com.amazonaws.services.cloudwatch.model.MetricAlarm;

public class CloudWatchMonitor
{
    public static void main(String[] args)
    {

//        final String awsAccessKey = "AKIAIGC5LZOEYXA2JLJA";
//        final String awsSecretKey = "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";
//        final String instanceId = "i-25b04ae7";
        final String awsAccessKey = "AKIAJBNM4MA7T4J5QVMA";
        final String awsSecretKey = "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";
        final String instanceId = "i-2c8c46ee";

        final AmazonCloudWatchClient client = client(awsAccessKey, awsSecretKey);
        final GetMetricStatisticsRequest request0 = request(instanceId,
                "CPUUtilization");
        final GetMetricStatisticsResult result0 = result(client, request0);
        System.out.println("CPU Utilization");
        toStdOut(result0, instanceId);

        final GetMetricStatisticsRequest request1 = request(instanceId,
                "NetworkIn");
        final GetMetricStatisticsResult result1 = result(client, request1);
        System.out.println("NetworkIn");
        toStdOut(result1, instanceId);

        final GetMetricStatisticsRequest request2 = request(instanceId,
                "NetworkOut");
        final GetMetricStatisticsResult result2 = result(client, request2);
        System.out.println("NetworkOut");
        toStdOut(result2, instanceId);

        DescribeAlarmsRequest requestAlarm = new DescribeAlarmsRequest();
        // Request. SetAlarmNames (Arrays. asList (" alarm name "));
        DescribeAlarmsResult alarms = client.describeAlarms(requestAlarm);

        List<MetricAlarm> list = alarms.getMetricAlarms();
        for (MetricAlarm a : list)
        {
            System.out.printf("%s\tstate=%s action=%s%n", a.getAlarmName(),
                    a.getStateValue(), a.getAlarmActions());
        }
    }

    private static AmazonCloudWatchClient client(final String awsAccessKey,
            final String awsSecretKey)
    {
        final AmazonCloudWatchClient client = new AmazonCloudWatchClient(
                new BasicAWSCredentials(awsAccessKey, awsSecretKey));
        client.setEndpoint("http://monitoring.us-west-1.amazonaws.com");
        return client;
    }

    private static GetMetricStatisticsRequest request(final String instanceId,
            String statName)
    {
        final long twentyFourHrs = 1000 * 60 * 60 * 24;
        final int oneHour = 60 * 60;
        final int fiveMin = 60;
        return new GetMetricStatisticsRequest()
                .withStartTime(new Date(new Date().getTime() - twentyFourHrs))
                .withNamespace("AWS/EC2")
                .withPeriod(fiveMin)
                .withDimensions(
                        new Dimension().withName("InstanceId").withValue(
                                instanceId)).withMetricName(statName)
                .withStatistics("Average", "Maximum", "SampleCount")
                .withEndTime(new Date());
    }

    private static GetMetricStatisticsResult result(
            final AmazonCloudWatchClient client,
            final GetMetricStatisticsRequest request)
    {
        return client.getMetricStatistics(request);
    }

    public static void printTreeMap(TreeMap treeMap)
    {
        Iterator iterator = treeMap.entrySet().iterator();

        while (iterator.hasNext())
        {
            Map.Entry entry = (Map.Entry) iterator.next();
            System.out.print("Date: " + entry.getKey() + " | Average: "
                    + entry.getValue() + "\r\n");
        }
        System.out.println();
    }

    private static void toStdOut(final GetMetricStatisticsResult result,
            final String instanceId)
    {
        TreeMap<Date, Double> map = new TreeMap<Date, Double>();
        for (final Datapoint dataPoint : result.getDatapoints())
        {
            map.put(dataPoint.getTimestamp(), dataPoint.getAverage());

            // System.out.println("------------------------------------------------------------------");
            // System.out.printf("%s instance's TimeStamp : %s%n", instanceId,
            // dataPoint.getTimestamp());
            // System.out.printf("%s instance's average CPU utilization : %s%n",
            // instanceId, dataPoint.getAverage());
            // System.out.printf("%s instance's max CPU utilization : %s%n",
            // instanceId, dataPoint.getMaximum());
            // System.out.printf("%s instance's sample count CPU utilization : %s%n",
            // instanceId, dataPoint.getSampleCount());
        }

        printTreeMap(map);
    }
}
