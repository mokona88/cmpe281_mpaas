/**
 * 
 */
package com.sjsu.cmpe281.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.amazonaws.services.ec2.model.Image;

/**
 * @author squall
 *
 */
public class DBInstance
{
    private Connection conn;
    private static final String DEFAULT_USERNAME= "cmpe281";
    private static final String DEFAULT_PASSWORD = "group8";
    private static final String DEFAULT_PORT = "3306";
    private static final String DEFAULT_IP = "127.0.0.1";
    private static final String DEFAULT_DB = "cmpe281";    
//    private static final String DEFAULT_URL = "jdbc:mysql://127.0.0.1:3306/cmpe281";
    private static final String PROTOCOL = "jdbc:mysql://";
    private static final String imageTable = "VMImage1";
    
    
    private String username, pass, port, ip, dbName;
    
    private static DBInstance singleInstance = null;
    
    public DBInstance()
    {
        username = DEFAULT_USERNAME; pass = DEFAULT_PASSWORD;
        dbName = DEFAULT_DB; port = DEFAULT_PORT; ip = DEFAULT_IP;
        openConnection();
    }
    
    public DBInstance(String ip, String DbName, String username, String password, String port)
    {
        this.ip = ip; dbName = DbName; this.username = username; pass = password;
        if ( (ip == null) || (DbName == null) || (username == null) || (password == null) )
        {
            System.err.println("Must provide <IP> <DB NAME> <USER> <PASS>");
            return;
        }
        if (port == null)
            port = DEFAULT_PORT;
        else
            this.port = port;
        openConnection();
    }
    
    private void openConnection()
    {
        // Setup connection
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(PROTOCOL + ip + ":" + port + "/" + dbName, username, pass);
        }catch (Exception e)
        {
            e.printStackTrace();
        }        
    }
    
    public void closeConnection()
    {
        try
        {
            if (conn != null)
                conn.close();            
        }
        catch (Exception e){}
    }
    
    public int addImageRecord(Image image)
    {
        String query = "INSERT INTO " + imageTable
                + " VALUES ('" + image.getArchitecture() + "','" + image.getCreationDate() + "','"
                + image.getImageId() + "','"
                + image.getImageLocation() + "','"
                + image.getImageType() + "','"
                + image.getKernelId() + "','"
                + image.getOwnerId() + "','"
                + image.getPlatform() + "','"
                + image.getProductCodes() + "','"
                + (image.getPublic() == true ? "1" : "0") + "','"
                + image.getRamdiskId() + "','"
                + image.getState() + "','"
                + image.getDescription() + "','"
                + image.getHypervisor() + "','"
                + image.getImageOwnerAlias() + "','"
                + image.getName() + "','"
                + image.getRootDeviceName() + "','"
                + image.getRootDeviceType() + "','"
                + image.getSriovNetSupport() + "','"
                + image.getVirtualizationType() + "');";
        int rs = 0;
        try
        {
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            conn = DriverManager.getConnection(PROTOCOL + ip + ":" + port + "/" + dbName, username, pass);
            Statement st = conn.createStatement();
            rs = st.executeUpdate(query);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return rs;
    }
    
    public static DBInstance getInstance()
    {
        if (singleInstance == null)
            singleInstance = new DBInstance();
        return singleInstance;
    }
    
    public static DBInstance getInstance(String ip, String DbName, String username, String password, String port)
    {
        if (singleInstance == null)
            singleInstance = new DBInstance(ip, DbName, username, password, port);
        return singleInstance;
    }
    
    public void executeQuery(String query)
    {
        try
        {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            
            System.out.println(rs.toString());
        }
        catch (Exception e ){}
    }
}
