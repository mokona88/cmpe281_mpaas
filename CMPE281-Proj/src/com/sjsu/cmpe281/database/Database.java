package com.sjsu.cmpe281.database;

import java.sql.*;

public class Database {
	
	Connection con = null;
	PreparedStatement ps = null;
	Statement stmt= null;
	ResultSet rs = null;
	
	String driverName = "com.mysql.jdbc.Driver";
	String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
	String user = "cmpe281";
	String password = "group8";
	
	public Database(){
		try{
			Class.forName(driverName).newInstance();
			con = DriverManager.getConnection(url,user,password);
			stmt = con.createStatement();
		if(!con.isClosed())
			System.out.println("Successfully connected!");
	
	} catch (SQLException e) {
		e.printStackTrace();
	} catch (InstantiationException e) {
		e.printStackTrace();
	} catch (IllegalAccessException e) {
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	}	
	}
	
	public int getTime(){
		String sql ="SELECT timediff(endTime, startTime) as diff FROM TestCase";
		int diff = 0;
		try {
			rs=stmt.executeQuery(sql);
		rs.next();
			if(rs!=null){
				diff=rs.getInt("diff");
			}
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return diff;
		
	}

}
