/**
 * 
 */
package com.sjsu.cmpe281.fileupload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 * @author squall
 *
 */
public class FileUploadServlet extends HttpServlet implements Servlet
{
    public FileUploadServlet()
    {
        // TODO Auto-generated constructor stub
        super();
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        // TODO Auto-generated method stub
        super.doGet(req, resp);
        PrintWriter out = resp.getWriter();
        HttpSession session = req.getSession();
        FileUploadListener listener = null;
        StringBuffer buffy = new StringBuffer();
        long bytesRead = 0, contentLength = 0;
        
        if (session == null)
            return;
        else
        {
            listener = (FileUploadListener)session.getAttribute("LISTENER");
            
            if (listener == null)
                return;
            else
            {
                bytesRead = listener.getBytesRead();
                contentLength = listener.getContentLength();
            }
            
            resp.setContentType("text/xml");
            buffy.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n");
            buffy.append("<response>\n");
            buffy.append("\t<byte_read>" + bytesRead + "</byte_read>\n");
            buffy.append("\t<content_length>" + contentLength + "</content_length>\n");
            
            if (bytesRead == contentLength)
            {
                buffy.append("\t<finished />\n");
                session.setAttribute("LISTENER", null);
            }else
            {
                long percentComplete = (bytesRead / contentLength) * 100;
                buffy.append("\t<percent_complete>" + percentComplete + "</percent_complete>\n");
                buffy.append("</response>\n");
                out.println(buffy.toString());
                out.flush();
                out.close();
            }
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        // TODO Auto-generated method stub
        super.doPost(req, resp);
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        FileUploadListener listener = new FileUploadListener();
        HttpSession session = req.getSession();
        session.setAttribute("LISTENER", listener);
        upload.setProgressListener(listener);
        List uploadItems = null;
        FileItem fileItem = null;
        String filepath = "/home/cmpe283/data/example/FileUploadServlet";
        try
        {
            uploadItems = upload.parseRequest((RequestContext) req);
            Iterator i = uploadItems.iterator();
            while (i.hasNext())
            {
                fileItem = (FileItem)i.next();
                if (fileItem.isFormField() == false)
                    if (fileItem.getSize() > 0)
                    {
                        File uploadedFile = null;
                        String myFullFileName = fileItem.getName(), myFileName = "", slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/";
                        int startIndex = myFullFileName.lastIndexOf(slashType);
                        myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
                        uploadedFile = new File(filepath, myFileName);
                        fileItem.write(uploadedFile);
                    }
            }
        }catch (FileUploadException e)
        {
            e.printStackTrace();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
