package com.sjsu.cmpe281.fileupload;

import org.apache.tomcat.util.http.fileupload.ProgressListener;

public class FileUploadListener implements ProgressListener
{

    private volatile long bytesRead = 0L, contentLength = 0L, item = 0L;
    
    public FileUploadListener()
    {
        // TODO Auto-generated constructor stub
        super();
    }
    
    @Override
    public void update(long aBytesRead, long aContentLength, int anItem)
    {
        // TODO Auto-generated method stub
        bytesRead = aBytesRead;
        contentLength = aContentLength;
        item = anItem;
    }

    public long getBytesRead()
    {
        return bytesRead;
    }

    public long getContentLength()
    {
        return contentLength;
    }

    public long getItem()
    {
        return item;
    }

}
