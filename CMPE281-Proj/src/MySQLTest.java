import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

//import com.amazonaws.services.rds.model.DBInstance;







import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.Image;
import com.sjsu.cmpe281.database.*;

/**
 * 
 */

/**
 * @author squall
 *
 */
public class MySQLTest
{

    private static final String username = "cmpe281";
    private static final String password = "group8";
    private static AmazonEC2 ec2;
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub
        Connection conn = null;
        
//        try
//        {
//            String str_link = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
////            String str_link = "jdbc:mysql://192.168.1.120:3306/cmpe281";
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            conn = DriverManager.getConnection(str_link, username, password);
//            System.out.println("Connection established!!!");
//            
//            Statement st = conn.createStatement();
//            ResultSet rs = st.executeQuery("select * from VMImage");
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        finally
//        {
//            if (conn != null)
//            {
//                try
//                {
//                    conn.close();
//                }catch (Exception e){ }
//            }
//        }
//        DBInstance instance = DBInstance.getInstance("192.168.1.120", "cmpe281", username, password, "3306");
//        instance.executeQuery("select * from VMImage");
        
        AWSCredentialsProvider credentialProvider = new ClasspathPropertiesFileCredentialsProvider();
        ec2 = new AmazonEC2Client(credentialProvider);
        ec2.setRegion(Region.getRegion(Regions.US_WEST_1));
        
        List<Image> images = ec2.describeImages().getImages();
        List<String> validImgIds = new ArrayList<String>();
        DBInstance instance = DBInstance.getInstance("192.168.1.120", "cmpe281", "cmpe281", "group8", "3306");
        for (Image image : images)
        {
            // System.out.println(image.getImageId() + '\t' + image.getName() + '\t' + image.getImageType() + '\t' + image.getKernelId() + '\t' + image.getState());
            instance.addImageRecord(image);
            if (image.getImageId().contains("ami-"))
            {
                validImgIds.add(image.getImageId());
                System.out.println(image.getImageType());
            }
        }
        instance.closeConnection();
    }

}
