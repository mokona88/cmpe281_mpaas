<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.DriverManager"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.util.*"%>
<%@ page import="com.sjsu.cmpe281.database.CloudWatchMonitor"%>
<%@page import="com.amazonaws.regions.Regions"%>
<%@page import="com.amazonaws.regions.Region"%>
<%@ page import="com.amazonaws.*"%>
<%@ page import="com.amazonaws.auth.*"%>
<%@ page import="com.amazonaws.services.ec2.*"%>
<%@ page import="com.amazonaws.services.ec2.model.*"%>
<%@ page import="com.amazonaws.services.s3.*"%>
<%@ page import="com.amazonaws.services.s3.model.*"%>
<%@ page import="com.amazonaws.auth.BasicAWSCredentials"%>
<%@ page import="com.amazonaws.services.cloudwatch.model.*"%>
<%@ page
	import="com.amazonaws.services.cloudwatch.AmazonCloudWatchClient"%>
<%@ page import="java.text.DecimalFormat"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Report</title>
<link href="style_1.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="bootstrap3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="bootstrap3.3.4/css/bootstrap-theme.min.css">
<script type="text/javascript" rel="stylesheet"
	src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" rel="stylesheet"
	src="bootstrap3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
	<%!String instanceName;
	Integer num_data = 0;%>
	<div id="wrapper">

		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<li class="sidebar-brand"><a href="index.jsp">Profile </a></li>
				<li><a href="services.jsp">Services</a></li>
				<li><a href="billing.jsp">Billing</a></li>
				<li><a href="report.jsp">Report</a></li>
			</ul>
		</div>
		<!-- Navigation bar -->
		<div id="page-content-wrapper">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<a href="#" class="navbar-brand">CMPE281 - MOBILE PLATFORM
							TESTING - <%=session.getAttribute("first_name")%> <%=session.getAttribute("last_name")%></a>
					</div>

					<ul class="nav navbar-right">
						<li class="dropdown"><a class="dropdown dropdown-toggle"
							data-toggle="dropdown" href="#"> <i
								class="glyphicon glyphicon-user"></i><span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li role="presentation"><a href="index.jsp" role="menuitem">Profile</a></li>
								<!-- link to user profile -->
								<li role="presentation"><a href="logout.jsp"
									role="menuitem">Logout</a></li>
							</ul></li>
					</ul>
				</div>
			</nav>
			<!-- End Navigation bar -->

			<!-- Page Content -->

			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1>Monitoring Report</h1>

						<%
							Connection con = null;
							PreparedStatement ps = null;
							ResultSet rs = null;

							String driverName = "com.mysql.jdbc.Driver";
							String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
							String user = "cmpe281";
							String dbpsw = "group8";
							String id = (session.getAttribute("id")).toString();
							String sql = "Select distinct userId, instanceName from EC2Instance e1, TestCase t1 where e1.instanceId=t1.instanceId AND t1.userId=?";

							try {
								Class.forName(driverName);
								con = DriverManager.getConnection(url, user, dbpsw);
								ps = con.prepareStatement(sql);
								ps.setString(1, id);
								rs = ps.executeQuery();

								while (rs.next()) {
									rs.getString("instanceName");
									instanceName = rs.getString("instanceName");
									System.out.println("user id is " + id);
									System.out.println("instance is " + instanceName);
									num_data++;
								}
							} catch (SQLException sqe) {
								out.println(sqe);
							}
						%>
						<%
							System.out.println(num_data);
							if (num_data != 0) {
								final String awsAccessKey = "AKIAIGC5LZOEYXA2JLJA";
								final String awsSecretKey = "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";
								final String instanceId = instanceName;
								System.out.println("instance name goes to CW is "
										+ instanceName);

								final long twentyFourHrs = 1000 * 60 * 60 * 24;
								final int oneHour = 60 * 60;

								final AmazonCloudWatchClient client = new AmazonCloudWatchClient(
										new BasicAWSCredentials(awsAccessKey, awsSecretKey));
								client.setEndpoint("http://monitoring.us-west-1.amazonaws.com");

								final GetMetricStatisticsRequest request0 = new GetMetricStatisticsRequest()
										.withStartTime(
												new Date(new Date().getTime() - twentyFourHrs))
										.withNamespace("AWS/EC2")
										.withPeriod(oneHour)
										.withDimensions(
												new Dimension().withName("InstanceId")
														.withValue(instanceId))
										.withMetricName("CPUUtilization")
										.withStatistics("Average", "Maximum", "SampleCount")
										.withEndTime(new Date());

								final GetMetricStatisticsResult result0 = client
										.getMetricStatistics(request0);
								System.out.println("CPU Utilization");

								TreeMap<Date, Double> map0 = new TreeMap<Date, Double>();
								for (final Datapoint dataPoint : result0.getDatapoints()) {
									map0.put(dataPoint.getTimestamp(), dataPoint.getAverage());
								}

								Iterator iterator0 = map0.entrySet().iterator();
						%>
						<h3 id="h3">CPU Utilization</h3>
						<table id=customers>
							<tr>
								<th>Time Stamp</th>
								<th>Average %</th>
							</tr>
							<%
								while (iterator0.hasNext()) {
										Map.Entry entry = (Map.Entry) iterator0.next();
							%>
							<tr>
								<td>
									<%
										out.println(entry.getKey());
									%>
								</td>
								<td>
									<%
										DecimalFormat dFormat = new DecimalFormat("0.00");
												String v = dFormat.format(entry.getValue());
												out.println(v);
									%>
								</td>
							</tr>
							<%
								}
							%>
						</table>
						<br />
						<%
							final GetMetricStatisticsRequest request1 = new GetMetricStatisticsRequest()
										.withStartTime(
												new Date(new Date().getTime() - twentyFourHrs))
										.withNamespace("AWS/EC2")
										.withPeriod(oneHour)
										.withDimensions(
												new Dimension().withName("InstanceId")
														.withValue(instanceId))
										.withMetricName("NetworkIn")
										.withStatistics("Average", "Maximum", "SampleCount")
										.withEndTime(new Date());

								final GetMetricStatisticsResult result1 = client
										.getMetricStatistics(request1);
								System.out.println("Network In");

								TreeMap<Date, Double> map1 = new TreeMap<Date, Double>();
								for (final Datapoint dataPoint : result1.getDatapoints()) {
									map1.put(dataPoint.getTimestamp(), dataPoint.getAverage());
								}

								Iterator iterator1 = map1.entrySet().iterator();
						%>
						<h3 id="h3">Network In</h3>
						<table id="customers">
							<tr>
								<th>Time Stamp</th>
								<th>Average Bytes</th>
							</tr>
							<%
								while (iterator1.hasNext()) {
										Map.Entry entry = (Map.Entry) iterator1.next();
							%>
							<tr>
								<td>
									<%
										out.println(entry.getKey());
									%>
								</td>
								<td>
									<%
										DecimalFormat dFormat = new DecimalFormat("0.00");
												String v = dFormat.format(entry.getValue());
												out.println(v);
									%>
								</td>
							</tr>
							<%
								}
							%>
						</table>
						<br />
						<%
							final GetMetricStatisticsRequest request2 = new GetMetricStatisticsRequest()
										.withStartTime(
												new Date(new Date().getTime() - twentyFourHrs))
										.withNamespace("AWS/EC2")
										.withPeriod(oneHour)
										.withDimensions(
												new Dimension().withName("InstanceId")
														.withValue(instanceId))
										.withMetricName("NetworkOut")
										.withStatistics("Average", "Maximum", "SampleCount")
										.withEndTime(new Date());

								final GetMetricStatisticsResult result2 = client
										.getMetricStatistics(request2);
								System.out.println("Network Out");

								TreeMap<Date, Double> map2 = new TreeMap<Date, Double>();
								for (final Datapoint dataPoint : result2.getDatapoints()) {
									map2.put(dataPoint.getTimestamp(), dataPoint.getAverage());
								}

								Iterator iterator2 = map2.entrySet().iterator();
						%>
						<h3 id="h3">Network Out</h3>
						<table id="customers">
							<tr>
								<th>Time Stamp</th>
								<th>Average Bytes</th>
							</tr>
							<%
								while (iterator2.hasNext()) {
										Map.Entry entry = (Map.Entry) iterator2.next();
							%>
							<tr>
								<td>
									<%
										out.println(entry.getKey());
									%>
								</td>
								<td>
									<%
										DecimalFormat dFormat = new DecimalFormat("0.00");
												String v = dFormat.format(entry.getValue());
												out.println(v);
									%>
								</td>
							</tr>
							<%
								}
								} else {
							%>
							<h3 id="h3">CPU Utilization</h3>
							<table id=customers>
								<tr>
									<th>Time Stamp</th>
									<th>Average %</th>
								</tr>
								<tr>
									<td></td>
									<td>NO DATA</td>
								</tr>
							</table>
							<br />
							<h3 id="h3">Network In</h3>
							<table id="customers">
								<tr>
									<th>Time Stamp</th>
									<th>Average Bytes</th>
								</tr>
								<tr>
									<td></td>
									<td>NO DATA</td>
								</tr>
							</table>
							<h3 id="h3">Network Out</h3>
							<table id="customers">
								<tr>
									<th>Time Stamp</th>
									<th>Average Bytes</th>
								</tr>
								<tr>
									<td></td>
									<td>NO DATA</td>
								</tr>
							</table>

							<%
								}
							%>
						</table>
						<br />
					</div>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->


</body>
</html>