<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.amazonaws.regions.Regions"%>
<%@page import="com.amazonaws.regions.Region"%>
<%@ page import="com.amazonaws.*"%>
<%@ page import="com.amazonaws.auth.*"%>
<%@ page import="com.amazonaws.services.ec2.*"%>
<%@ page import="com.amazonaws.services.ec2.model.*"%>
<%@ page import="com.amazonaws.services.s3.*"%>
<%@ page import="com.amazonaws.services.s3.model.*"%>

<%!
	private AmazonEC2		ec2;
	private AmazonS3		 s3;
	
%>

<%
	if (ec2 == null)
	{
	    AWSCredentialsProvider credentialProvider = new ClasspathPropertiesFileCredentialsProvider();
	    ec2 = new AmazonEC2Client(credentialProvider);
	    ec2.setRegion(Region.getRegion(Regions.US_WEST_1));
	    s3  = new AmazonS3Client(credentialProvider);
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="style.css" rel="stylesheet">
<title>Instances</title>
</head>

<body>
	<div id="wrapper">

		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<li class="sidebar-brand"><a href="index.jsp">Profile </a></li>
				<li><a href="Instances.jsp">Services</a></li>
				<li><a href="repository.jsp">Repository</a></li>
				<li><a href="billing.jsp">Billing</a></li>
				<li><a href="report.jsp">Report</a></li>
				<li><a href="logout.jsp">Logout</a></li>
			</ul>
		</div>


		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div id="list_instance">
							<table border="1px">
								<tr>
									<td>Name</td>
									<td>AMI ID</td>
									<td>IP</td>
									<td>Type</td>
									<td>State</td>
									<td>Status</td>
								</tr>
								<%
									for (Reservation reservation : ec2.describeInstances().getReservations())
									{
									    for (Instance instance : reservation.getInstances())
									    {
									    %>
								<tr>
									<td><%= instance.getImageId() %></td>
									<td><%= instance.getInstanceId() %></td>
									<td><%= instance.getPrivateIpAddress() %></td>
									<td><%= instance.getInstanceType() %></td>
									<td><%= instance.getState().getName() %></td>
									<td><%= instance.getState().getCode() %></td>
								</tr>
								<%
									    }
									}
								%>
							</table>
						</div>
						<div id="instance_monitor_data"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->
</body>
</html>