/**
 * 
 */
function request2EC2() {
	var ec2 = new AWS.EC2();
	ec2.acceptVpcPeeringConnection(params, function(err, data) {
		if (err)
			console.log(err, err.stack); // an error occurred
		else
			console.log(data); // successful response
	});
	return ec2;
}

function launchInstances() {
	location.href = 'launchinstance.jsp';
}

function showSDK() {
	var addedSection = '<option value=""></option>';
	var deviceOption = document.getElementById('device-selector').value;
	if (deviceOption == 'android')
		addedSection = '<option value="19">Api 19</option><option value="20">Api 20</option><option value="21">Api 21</option>';
	else if (deviceOption == 'windows')
		addedSection = '<option value="win8">Windows Phone 8</>';
	else if (deviceOption == "apple")
		addedSection = '<option value="6">iOS 6</option><option value="7">iOS 7</option><option value="8">iOS 8</option>';
	else
		addedSection = '<option value=""></option>';
	var output = window.document.getElementById('sdk-target');
	output.innerHTML = addedSection;
}

function updateProgressBar(progressBarValue) {
	$("#progressbar").progressbar("option", "value", progressBarValue);
}

function ajaxFunction() {
	var url = "fileupload/FileUploadServlet";
	var req;

	if (window.XMLHttpRequest) {
		req = new XMLHttpRequest();
		try {
			req.onreadystatechange = funcReadyStateChange;
			req.open("GET", url, true);
		} catch (e) {
			alert(e);
		}
		req, send(null);
	} else if (window.ActiveXObject) {
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req) {
			req.onreadystatechange = funcReadyStateChange;
			req.open("GET", url, true);
			req.send();
		}
	}
}

function funcReadyStateChange(req) {
	if (req.readyState == 4) {
		if (req.status == 200) {
			var xml = req.responseXML;
			// get the current read bytes and the contents length
			var responseNode = xml.getElementsByTagName("response")[0];
			var noOfBytesRead = responseNode
					.getElemementsByTagName("bytes_read")[0].childNodes[0].nodeValue;
			var totalNoOfBytes = responseNode
					.getElementsByTagName("content_length")[0].childNodes[0].nodeValue;

			progressBarValue = noOfBytesRead / totalNoOfBytes * 100;

			document.getElementById("status").style.display = "block";

			if (progressBar < 100) {
				window.setTimeout("ajaxFunction();", 100);
				window.setTimeout("updateProgressBar(progressBarValue);", 100);
			} else {
				alert("Done");
				window.setTimeout("updateProgressBar(100);", 100);
				document.getElementById("progressbarWrapper").style.display = "none";
				document.getElementById("status").style.display = "none";
			}
		} else {
			alert(req.statusText);
		}
	}
}

//function assignInstance(userId) {
//	/* assume that the user did select proper info */
//	var deviceType = document.getElementById('device-selector').value;
//	var sdkTarget = document.getElementById('sdk-target').value;
//
//	document.getElementById('tab1').innerHTML = userId;
//	console.log(userId);
//	console.log("Device: " + deviceType);
//	console.log("SDK: " + sdkTarget);
//
//	var request;
//	try {
//		request = new XMLHttpRequest();
//		request.onreadystatechange = function() {
//			if (request.readyState == 4 && request.status == 200) {
//				document.getElementById("tab1").innerHTML = request.responseText;
//			}
//		};
//		request.open("GET", "./device_query.jsp?device=" + deviceType + "&sdk="
//				+ sdkTarget + "&userId=" + userId, true);
//		request.send();
//	} catch (e) {
//		alert(e);
//	}
//}

function assignInstance(userid)
{
    console.log("\n" + userid + "\n");
    
    var request;
    try
    {
        request = new XMLHttpRequest();
        request.onreadystatechange = updateTab1AndRefreshPage(request);
        request.open("GET", "./device_query.jsp?userId=" + userid, true);
        request.send();
    }catch (e)
    {
        alert(e);
    }
    
}

function updateTab1AndRefreshPage(request) {
	if (request.readyState == 4 && request.status == 200) {
		document.getElementById("tab1").innerHTML = request.responseText;
		console.log(request.responsText);
	}
}

function assignApp(userid) {
	var inId = document.getElementById("app-device-selector").value;
	var dataPath = document.getElementById("file-upload").value;
	var output = document.getElementById("test-app");
	var instanceId = document.getElementById("app-device-selector").value;
	var filename = "";
	if (dataPath) {
		var startIndex = (dataPath.indexOf('\\') >= 0) ? dataPath.indexOf('\\') : dataPath.indexOf('/');
		filename = dataPath.substring(startIndex + 1);
		filename.replace("fakepath","");
		console.log("\n File Upload: " + filename);
	}
	var request;
	try
	{
		request = new XMLHttpRequest();
		request.onreadystatechange = addAppAndRefreshPage(request);
		request.open("GET", "./add_app.jsp?appName="+filename+"&userid=" + userid + "&instanceId="+instanceId, true);
		request.send();
	}catch (e)
	{
		alert(e);
	}
}

function addAppAndRefreshPage(request)
{
	if (request.readyState == 4 && request.status == 200)
	{
		document.getElementById("test-app").innerHTML = request.responseText;
	}
}

function terminateConnection(userid, instanceid)
{
    console.log("\nuserid = " + userid + "\ninstanceId = " + instanceid );
    
    var request;
    try
    {
        request = new XMLHttpRequest();
        request.onreadystatechange = updateTab1AndRefreshPage(request);
        request.open("GET", "./device_remove.jsp?userid=" + userid + "&instanceid=" + instanceid);
        request.send();
    }
    catch (e)
    {
        alert(e);
    }
}