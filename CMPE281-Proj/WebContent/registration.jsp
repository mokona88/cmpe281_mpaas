<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SignUp</title>
<link href="style_1.css" rel="stylesheet">
</head>
<body id="main">
	<div class="header">WELCOME TO MPaSS</div>
	<div id="content">
		<form method="post" action="checkregistration.jsp">
			<table class="center">
				<tr>
					<td>Email</td>
					<td><input maxlength="20" size="12" name="email" type="text" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input maxlength="20" size="12" name="password"
						type="password" /></td>
				</tr>
				<tr>
					<td>Retype Password</td>
					<td><input maxlength="20" size="12" name="retype"
						type="password" /></td>
				</tr>
				<tr>
					<td>First Name</td>
					<td><input maxlength="20" size="12" name="first_name"
						type="text" /></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td><input maxlength="20" size="12" name="last_name"
						type="text" /></td>
				</tr>
				<tr>
					<td></td>
					<td><input class="myButton" type="submit" value="Register" /></td>
				</tr>
				<tr>
					<td></td>
					<td><a href='home.jsp'>Back to Login Page.</a></td>
				</tr>
			</table>
		</form>
		<br />
	</div>
</body>
</html>