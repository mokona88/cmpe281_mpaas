<%@page import="java.io.PrintStream"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!-- 1. Add an entry of app to the app table -->

<%!
String add_query = "";
String update_query = "";
String view_query = "";
%>

<%
String driver = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
String user = "cmpe281";
String password = "group8";
Connection conn = null;
PreparedStatement statement = null;
ResultSet rs = null;
String userid = request.getParameter("userid");
String app_name = request.getParameter("appName");
app_name = app_name.replace("fakepath", "");
String instanceId = request.getParameter("instanceId");
String appid = "";
/* if (platform.equals("iOS"))
    device_info = "iPhone";
else if (platform.equals("android"))
    device_info = "Android";
else
    device_info = "Windows";
String sdk = request.getParameter("sdk");
 */
try
{
    Class.forName(driver);
    conn = DriverManager.getConnection(url, user, password);
    
    app_name = app_name.replace("\\", "");
    add_query = "INSERT INTO MobileApp VALUES (null, '" + app_name + "', '" + userid + '/' + app_name + "')";
    statement = conn.prepareStatement(add_query);
    statement.executeUpdate();
    
    view_query = "SELECT AppId FROM MobileApp WHERE appPath='" + userid + '/' + app_name +"'";
    statement = conn.prepareStatement(view_query);
    rs = statement.executeQuery();
    while (rs.next())
    {
        appid = new Integer(rs.getInt("AppId")).toString();
        break;
    }
    if (appid == null || appid.equals(""))
    {
        System.out.println("Cannot find the specified app entry");
    }
    add_query = "UPDATE TestCase SET AppId="+appid+" WHERE userId="+userid+" AND instanceId="+instanceId;
    statement = conn.prepareStatement(add_query);
    statement.executeUpdate();
    
    view_query = "SELECT userId, TestCase.AppId as appId, appPath,"
            + " AppName, instanceId FROM TestCase, MobileApp WHERE TestCase.userId = " + userid + " AND TestCase.AppId = MobileApp.AppId";
    statement = conn.prepareStatement(view_query);
    rs = statement.executeQuery();
%>
                                        <tr>
                                            <td>AppID</td>
                                            <td>App Path</td>
                                            <td>App Name</td>
                                            <td>DeviceID</td>
                                        </tr>
<%
    while (rs.next())
    {
%>
        <tr>
        	<td><%= rs.getInt("appId") %></td>
        	<td><%= rs.getString("appPath") %></td>
        	<td><%= rs.getString("appName") %></td>
        	<td><%= rs.getInt("instanceId") %></td>
        </tr>
<%
    }
    view_query = "SELECT public_ip FROM EC2Instance WHERE instanceId=" + instanceId;    
    statement = conn.prepareStatement(view_query);
    rs = statement.executeQuery();
    String public_ip = "";
    while (rs.next())
    {
        public_ip = rs.getString("public_ip");
    }
    String [] cmd = {
            "scp", "-i", "/Users/squall/Dropbox/Tmp/keys/james_private_key.pem",
            "/Users/squall/Dropbox/Tmp/Android_GeoQuiz.apk",
            "ubuntu@" + public_ip + ":~/Desktop/"
    };
    
    Runtime rt = Runtime.getRuntime();
    // rt.exec("scp -i /Users/squall/Dropbox/Tmp/keys/james_private_key.pem /Users/squall/Dropbox/Tmp/Android_GeoQuiz.apk ubuntu@" + public_ip + ":~/Desktop");
    Process pb = new ProcessBuilder(cmd[0], cmd[1], cmd[2], cmd[3],cmd[4]).start();
    PrintStream ptrStream = new PrintStream(pb.getOutputStream());
    ptrStream.println(); ptrStream.close();
}
catch (Exception e)
{
    e.printStackTrace();
}
 
%>

<!-- 2. Modify entry on TestCase table -->