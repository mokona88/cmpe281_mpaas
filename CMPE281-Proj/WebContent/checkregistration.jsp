<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
<link href="style_1.css" rel="stylesheet">
</head>
<body>
	<%
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String dbemail;

		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String first_name = request.getParameter("first_name");
		String last_name = request.getParameter("last_name");

		String driverName = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
		String user = "cmpe281";
		String dbpsw = "group8";

		if ((!(email.equals(null) || email.equals("")) && !(password
				.equals(null) || password.equals("")))) {
			String sql = "select * from users where email = ?";

			try {
				Class.forName(driverName);
				con = DriverManager.getConnection(url, user, dbpsw);
				ps = con.prepareStatement(sql);
				ps.setString(1, email);
				rs = ps.executeQuery();

				if (rs.next()) {

					dbemail = rs.getString("email");

					if (email.equals(dbemail)) {
					%>
						<p class="msg">User already exists with <%=email%>. Please try again.</p>

						<%
							getServletContext().getRequestDispatcher("/registration.jsp").include(
										request, response);		
					}
				} else {

					String insertSql = "insert into users (email,password,first_name,last_name)"
							+ " VALUES(?,?,?,?)";
					ps = con.prepareStatement(insertSql);
					ps.setString(1, email);
					ps.setString(2, password);
					ps.setString(3, first_name);
					ps.setString(4, last_name);
					ps.executeUpdate();
					response.sendRedirect("home.jsp");
				}
			} catch (Exception e) {
				System.out.print(e);
				e.printStackTrace();
			}

		} else {
		%>
			<p class="msg">Please provide all the details</p>
			<%
				getServletContext().getRequestDispatcher("/registration.jsp").include(
							request, response);	
		}
	%>
</body>
</html>