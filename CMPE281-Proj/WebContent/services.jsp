<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="style1.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="bootstrap3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="bootstrap3.3.4/css/bootstrap-theme.min.css">
<script type="text/javascript" rel="stylesheet" src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" rel="stylesheet" src="bootstrap3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="functions.js"></script>
<title>Instances</title>
</head>

<body>
<%!
String dPlatform = "Test";
int dsdk = 0;
String dIp = "0.0.0.0";
             		String driver = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
String user = "cmpe281";
String password = "group8";
String view_query = "";
Connection conn = null;
PreparedStatement statement = null;
ResultSet rs = null;
%>
<%
String userId = (String)session.getAttribute("id");
%>
	<div id="wrapper">

		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<li class="sidebar-brand"><a href="index.jsp">Profile</a></li>
				<li><a href="services.jsp">Services</a></li>
				<li><a href="billing.jsp">Billing</a></li>
				<li><a href="report.jsp">Report</a></li>
			</ul>
		</div>


		<!-- Page Content -->
		<div id="page-content-wrapper">
        	
            <!-- Navigation bar -->
            <nav class="navbar navbar-default" role="navigation">
            	<div class="container-fluid">
                	<div class="navbar-header">
                        <a href="#" class="navbar-brand">CMPE281 - MOBILE PLATFORM TESTING - <%= session.getAttribute("first_name") %> <%= session.getAttribute("last_name") %></a>
                    </div>
                    
                    <ul class="nav navbar-right">
                    	<li class="dropdown">
                        	<a class="dropdown dropdown-toggle" data-toggle="dropdown" href="#">
                            	<i class="glyphicon glyphicon-user"></i><span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                            	<li role="presentation"><a href="index.jsp" role="menuitem">Profile</a></li> <!-- link to user profile -->
                            	<li role="presentation"><a href="logout.jsp" role="menuitem">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- End Navigation bar -->

            <!-- Page content -->
            <div class="panel panel-default">
                <div class="panel-heading">
                <!-- Add button -->
                  	<button class="navbar-btn btn btn-info glyphicon-plus" type="button" onclick="assignInstance(<%= session.getAttribute("id") %>)"> Add A Device</button>
                    <button class="navbar-btn btn btn-info glyphicon-plus" type="button" data-toggle="modal" data-target="#modal-add-app"> Submit an App</button>
                <!-- End of add button -->
                
                    <!-- Modal Windows: Adding new device lightbox -->
	                <!-- End of Modal Windows -->
                	<!-- Modal Windows: Adding a new Test Case -->
                	<div class="modal fade" id="modal-add-app" tabindex="-1" role="dialog">
                    	<div class="modal-dialog modal-md">
                        	<div class="modal-content">
                            	<!-- Header -->
                                <div class="modal-header">
                                	<button type="button" class="close" data-dismiss="modal">&times;</button> <!-- Close Modal Windows button -->
                                    <h4 class="modal-title">Adding a Mobile-App</h4>
                                </div>
                                <!-- Body -->
                                <div class="modal-body">
	                                	<form role="form" enctype="multipart/form-data" method="post"
	                                		action="services.jsp"
	                                	>
	                                    	<div class="form-group">
	                                            <label>Select a file to upload:</label>
	                                            <input type="file" name="file-upload" id="file-upload" size="50" value="Select apk..."/> <br />
	                                            <label for="app-device-selector">Assign to Device:</label>
	                                            <select id="app-device-selector">
	                                            <%
	                                            try
	                                            {
	                                                view_query = "SELECT instanceId, public_ip from EC2Instance join TestCase using (instanceId) WHERE userId=" + userId;
	                                                Class.forName(driver);
	                                                conn = DriverManager.getConnection(url, user, password);
	                                                statement = conn.prepareStatement(view_query);
	                                                rs = statement.executeQuery(view_query);
	                                                while (rs.next())
	                                                {
	                                            %>
	                                            		<option value="<%= rs.getInt("instanceId") %>" > IP: <%= rs.getString("public_ip") %> </option>
	                                            <%
	                                                }
	                                            }
	                                            catch (Exception e)
	                                            {
	                                                e.printStackTrace();
	                                            }
	                                            %>
	                                            </select>
                                                <%
                                                   /*  String saveFile = new String();
                                                    String contentType = request.getContentType();
                                                    if (contentType != null && contentType.indexOf("multipart/form-data") >= 0)
                                                    {
                                                        DataInputStream in = new DataInputStream(request.getInputStream());
                                                        int formDataLength = request.getContentLength();
                                                        byte dataBytes[] = new byte[formDataLength];
                                                        int byteRead = 0, totalBytesRead = 0;
                                                        
                                                        while (totalBytesRead < formDataLength)
                                                        {
                                                            byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                                                            totalBytesRead += byteRead;
                                                        }
                                                        
                                                        String file = new String(dataBytes);
                                                        saveFile = file.substring(file.indexOf("filename=\"") + 10);
                                                        saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                                                        saveFile = saveFile.substring(saveFile.lastIndexOf('/') + 1, saveFile.indexOf('\"'));
                                                        
                                                        int lastIndex = contentType.lastIndexOf("=");
                                                        String boundary = contentType.substring(lastIndex + 1, contentType.length());
                                                        int pos;
                                                        pos = file.indexOf("filename=\"");
                                                        pos = file.indexOf('\n', pos) + 1;
                                                        pos = file.indexOf('\n', pos) + 1;
                                                        pos = file.indexOf('\n', pos) + 1;
                                                        
                                                        int boundaryLocation = file.indexOf(boundary,pos) - 4;
                                                        int startPos = (file.substring(0, pos)).getBytes().length;
                                                        int endPos = (file.substring(0, boundaryLocation)).getBytes().length;
                                                        
                                                        
                                                        saveFile = "/Users/squall/Desktop" + saveFile;
                                                        
                                                        File ff = new File(saveFile);
                                                        try
                                                        {
                                                            FileOutputStream fileOut = new FileOutputStream(ff);
                                                            fileOut.write(dataBytes, startPos, endPos - startPos);
                                                            fileOut.flush();
                                                            fileOut.close();
                                                        }catch (Exception e)
                                                        {
                                                            out.println(e);
                                                        }
                                                    } */
                                                    
                                                    
                                                %>
	                                            <!-- <a href="#"  class="btn btn-primary" data-dismiss="modal" onClick="">Upload File</a> -->
                                                <input type="submit" value="Upload" class="btn btn-primary" onClick="assignApp(<%= session.getAttribute("id") %> )" />
	                                        </div>
	                                    </form>
                                    <br />
                                    <!--
                                    <div class="progress" id="progressbarWrapper">
                                    	<div class="progress-bar" id="progressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                    		<span class="sr-only">0% complete</span>
                                    	</div>
                                    </div>
                                    <br />
                                    
                                    <div id="status">
                                    	<table width="100%">
                                    		<tr> <td nowrap="nowrap"> <div id="percentDone"></div> </td> </tr>
                                    	</table>
                                    	<table width="100%">
                                    		<tr> <td nowrap="nowrap"> <div id="bytesRead"></div> </td> </tr>
                                    	</table>
                                    	<table width="100%">
                                    		<tr> <td nowrap="nowrap"> <div id="totalNoOfBytes"></div> </td> </tr>
                                    	</table>
                                    </div>
                                    -->
                                </div>
                                <script>
                                	var progressBarValue = 0;
                                	$(document).ready(function()
                                	{
/*                                 		$("progressbar").ariavaluenow = 0;
                                		document.getElementById("progressbarWrapper").style.display = "none";
 */                                		// document.getElementById("status").style.display = "none";
                                	});
                                </script>
                                
                                <!-- Footer -->
                                <div class="modal-footer">
                                	<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Modal Windows -->
                </div>
                <!-- End of Panel Heading -->
                <div class="panel-body">
                	
                	<div class="tabbable tabs-left">
                    	<ul class="nav nav-tabs">
                        	<li class="active"><a href="#tab1" data-toggle="tab"><i class="glyphicon glyphicon-phone"></i>Device</a></li>
                            <li><a href="#tab2" data-toggle="tab">Mobile Application</a></li>
                        </ul>
                        <div class="tab-content">
                                <!-- Start of first tab -->
                            <div id="tab1" class="tab-pane in active">
                            <%
								
								view_query = "SELECT userId, TestCase.instanceId as iId, public_ip FROM TestCase, EC2Instance WHERE userId = " + userId + " AND TestCase.instanceId = EC2Instance.instanceId";
								
								try
								{
								    Class.forName(driver);
								    conn = DriverManager.getConnection(url, user, password);
								    statement = conn.prepareStatement(view_query);
								    rs = statement.executeQuery();
									rs = statement.executeQuery(view_query);
									while (rs.next())
									{
									    // For debug only
									    // end of debug
								%>
                                <div class="panel panel-body">
                                    <!-- Add all needed code to this part -->
                                    <div class="col-xs-12 panel panel-default">
                                        <div class="col-xs-2">
                                            <br /> <a href="#"><img src="images/android_device.png" width="100" height="100" ></a>
                                        </div>
                                        
                                        <div class="col-xs-2 col-xs-offset-2">
                                        	<br /> <br />
                                            DeviceIP: <%= rs.getString("public_ip") %> <br />
                                            DevicePort: 5901
                                        </div>

                                        <div class="col-xs-2">
                                            <br/><br/>
                                            <div id="device-state">
                                                <img src="images/device-online.png" width="45" height="45"> <br/>
                                                <span class="form-group">Running</span>
                                            </div>
                                            <br/><br/>
                                        </div>
                                        <div class="col-xs-2">
                                        <br /><br /><br />
                                        	<button class="navbar-btn btn btn-primary" type="button" onClick='terminateConnection( <%= rs.getInt("userId") %> , <%= rs.getInt("iId") %>)'>Dispose</button>                                        
                                        </div>
                                    </div>

                                </div>
								<%		
									}
									conn.close();
								}catch (Exception e)
								{
								    e.printStackTrace();
								}

                            %>
                            </div>
                                <!-- End of first tab -->
                                
                            <div id="tab2" class="tab-pane">
                                <!-- Start of second tab -->
                                <div class="panel panel-body">
                                    <!-- Add all needed code to this part -->
                                    <table id="test-app" border="1">
                                        <tr>
                                            <td>AppID</td>
                                            <td>App Path</td>
                                            <td>App Name</td>
                                            <td>DeviceID</td>
                                        </tr>
                                <%
                                view_query = "SELECT userId, TestCase.AppId as appId, appPath, AppName, instanceId FROM TestCase, MobileApp WHERE userId = " + userId + " AND TestCase.AppId = MobileApp.AppId";
                                try
                                {
                                    Class.forName(driver);
                                    conn = DriverManager.getConnection(url, user, password);
                                    statement = conn.prepareStatement(view_query);
                                    rs = statement.executeQuery();
                                    while (rs.next())
                                    {
                                        // For debug only
                                        System.out.println(rs.getInt("appId") + '\n' + rs.getString("appPath") + '\n' + rs.getString("AppName") + '\n' + rs.getInt("instanceId"));
								%>
                                        <tr>
                                            <td><%= rs.getInt("appId") %></td>
                                            <td><%= rs.getString("appPath") %></td>
                                            <td><%= rs.getString("AppName") %></td>
                                            <td><%= rs.getInt("instanceId") %></td>
                                        </tr>
								<%    
                                    }
                                    conn.close();
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                                %>
                                    </table>
                                </div>
                                <!-- End of second tab content -->
                            </div>
                        </div>
                    </div>
                    
                    
                    <!-- Content --
                    <div class="row">
                        <div class="col-xs-2">                    
                            <!-- Sidebar section --
                            <div class="panel panel-default">
                                <div class="row">
                                    <a class="col-xs-8 col-xs-offset-2 btn btn-default" href="#" onClick="">Device</a>
                                </div>
                                <div class="row">
                                    <a class="col-xs-8 col-xs-offset-2 btn btn-default" href="#" onClick="">Tests</a>
                                </div>
                            </div>
                            <!-- End sidebar --                       
                        </div>
                    </div>
                    <!-- End of content -->
                </div>            
            </div>
        
<!--			<div class="container-fluid">
            
            	<!-- Navbar --
                <div class="navbar navbar-default">
                    <div class="navbar navbar-brand col-xs-12"> <a class="btn btn-info glyphicon-plus"> Add A Device</a> </div>
                </div>
				<!-- End of Navbar --
				
                
                <!-- End of container-fluid --
			</div> -->
            
		</div>
            
            
            
            
		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->
<!-- 	<script type="text/javascript" rel="stylesheet" src="js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" rel="stylesheet" src="bootstrap3.3.4/js/bootstrap.min.js"></script> -->
</body>
</html>



<!--
    Remaining Tasks:
    1. Link to user profile page
    2. 
    -->