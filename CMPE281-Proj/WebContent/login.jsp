<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="style_1.css" rel="stylesheet">
<title>Login</title>
</head>
<body id="body1">
	<%!String dbemail;
	String dbpassword;
	String dbfirst_name;
	String dblast_name;
	String dbuser_id;
	%>
	<%
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String driverName = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
		String user = "cmpe281";
		String dbpsw = "group8";

		String sql = "select * from users where email=? and password=?";

		String id=request.getParameter("id");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String first_name = request.getParameter("first_name");
		String last_name = request.getParameter("last_name");

		if ((!(email.equals(null) || email.equals("")) && !(password
				.equals(null) || password.equals("")))) {
			try {
				Class.forName(driverName);
				con = DriverManager.getConnection(url, user, dbpsw);
				ps = con.prepareStatement(sql);
				ps.setString(1, email);
				ps.setString(2, password);
				rs = ps.executeQuery();
				if (rs.next()) {
					dbemail = rs.getString("email");
					dbpassword = rs.getString("password");
					dbfirst_name = rs.getString("first_name");
					dblast_name = rs.getString("last_name");
					dbuser_id = rs.getString("id");
					
					if (email.equals(dbemail) && password.equals(dbpassword)) {
						session.setAttribute("first_name", dbfirst_name);
						session.setAttribute("last_name", dblast_name);
						session.setAttribute("email", dbemail);
						session.setAttribute("id", dbuser_id);
						response.sendRedirect("index.jsp");
					}
				} else
					response.sendRedirect("error.jsp");
				rs.close();
				ps.close();
			} catch (SQLException sqe) {
				out.println(sqe);
			}
		} else {
	%>
		<p class="msg">Missing e-mail AND/OR password. Please try again.</p>

	<%
		getServletContext().getRequestDispatcher("/home.jsp").include(
					request, response);
		}
	%>
</body>
</html>