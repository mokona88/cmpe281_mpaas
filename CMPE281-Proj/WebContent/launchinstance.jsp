<%@page import="com.sjsu.cmpe281.database.DBInstance"%>
<%@page import="com.amazonaws.services.ec2.model.InstanceType"%>
<%@page import="com.amazonaws.regions.Regions"%>
<%@page import="com.amazonaws.regions.Region"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.amazonaws.services.ec2.model.RunInstancesResult"%>
<%@page import="com.amazonaws.services.ec2.model.Image"%>
<%@page import="java.util.List"%>
<%@page import="com.amazonaws.services.ec2.model.RunInstancesRequest"%>
<%@page import="com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider"%>
<%@page import="com.amazonaws.auth.AWSCredentialsProvider"%>
<%@page import="com.amazonaws.services.ec2.AmazonEC2Client"%>
<%@page import="com.amazonaws.services.ec2.AmazonEC2"%>
<%!
	private AmazonEC2 ec2;
	
%>

<%
	AWSCredentialsProvider credentialProvider = new ClasspathPropertiesFileCredentialsProvider();
	ec2 = new AmazonEC2Client(credentialProvider);
	ec2.setRegion(Region.getRegion(Regions.US_WEST_1));
	
	List<Image> images = ec2.describeImages().getImages();
    List<String> validImgIds = new ArrayList<String>();
    DBInstance instance = DBInstance.getInstance("192.168.1.120", "cmpe281", "cmpe281", "group8", "3306");
    for (Image image : images)
    {
        // System.out.println(image.getImageId() + '\t' + image.getName() + '\t' + image.getImageType() + '\t' + image.getKernelId() + '\t' + image.getState());
        instance.addImageRecord(image);
       /*  if (image.getImageId().contains("ami-") && image.getName().contains("ubuntu-trusty-14.04"))
        {
            validImgIds.add(image.getImageId());
            System.out.println(image.getImageId() + "\t" + image.getName() + "\t" + image.getImageLocation());
        } */
    }
    instance.closeConnection();
	
	String result = "";
	for (int i = 0; i < validImgIds.size(); i++)
	{
	    String imageId = validImgIds.get(i);
	    try
	    {
	        result = launchAnInstance(imageId).toString();
	        break;
	    }catch (Exception e)
	    {
	        System.out.println("Move on to the next imageId.");
	    }
	}
	%>
	<%= result
%>
	
<%!
	private RunInstancesResult launchAnInstance(String imageId) throws Exception
	{
	 	RunInstancesRequest runInstancesRequest = new RunInstancesRequest(imageId, new Integer("1"), new Integer("1"));
	 	runInstancesRequest.setInstanceType(InstanceType.T2Micro);
		RunInstancesResult runRes = ec2.runInstances(runInstancesRequest);
		return runRes;
	}
%>