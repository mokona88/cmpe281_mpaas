<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="style.css" rel="stylesheet">
<title>Login Error</title>
</head>
<body>
		<p class="msg">Sorry, your record is invalid or not available.</p>

	<%
		getServletContext().getRequestDispatcher("/home.jsp").include(
				request, response);
	%>
</body>
</html>