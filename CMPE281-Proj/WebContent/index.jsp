<%@page import="com.amazonaws.regions.Regions"%>
<%@page import="com.amazonaws.regions.Region"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="com.amazonaws.*"%>
<%@ page import="com.amazonaws.auth.*"%>
<%@ page import="com.amazonaws.services.ec2.*"%>
<%@ page import="com.amazonaws.services.ec2.model.*"%>
<%@ page import="com.amazonaws.services.s3.*"%>
<%@ page import="com.amazonaws.services.s3.model.*"%>
<%@ page import="com.amazonaws.services.dynamodbv2.*"%>
<%@ page import="com.amazonaws.services.dynamodbv2.model.*"%>



<%!// Share the client objects across threads to
	// avoid creating new clients for each web request
	private AmazonEC2 ec2;
	private AmazonS3 s3;
	private AmazonDynamoDB dynamo;%>

<%
	/*
	 * AWS Elastic Beanstalk checks your application's health by periodically
	 * sending an HTTP HEAD request to a resource in your application. By
	 * default, this is the root or default resource in your application,
	 * but can be configured for each environment.
	 *
	 * Here, we report success as long as the app server is up, but skip
	 * generating the whole page since this is a HEAD request only. You
	 * can employ more sophisticated health checks in your application.
	 */
	if (request.getMethod().equals("HEAD"))
		return;
%>

<%
	if (ec2 == null) {
		AWSCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();
		ec2 = new AmazonEC2Client(credentialsProvider);
		ec2.setRegion(Region.getRegion(Regions.US_WEST_1));
		s3 = new AmazonS3Client(credentialsProvider);
		dynamo = new AmazonDynamoDBClient(credentialsProvider);
	}
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Mobile PaaS</title>

<link href="style_1.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="bootstrap3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="bootstrap3.3.4/css/bootstrap-theme.min.css">
<script type="text/javascript" rel="stylesheet"
	src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" rel="stylesheet"
	src="bootstrap3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
	<div id="wrapper">

		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<li class="sidebar-brand"><a href="index.jsp">Profile </a></li>
				<li><a href="services.jsp">Services</a></li>
				<li><a href="billing.jsp">Billing</a></li>
				<li><a href="report.jsp">Report</a></li>
			</ul>
		</div>
		<div id="page-content-wrapper">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<a href="#" class="navbar-brand">CMPE281 - MOBILE PLATFORM
							TESTING - <%=session.getAttribute("first_name")%> <%=session.getAttribute("last_name")%></a>
					</div>

					<ul class="nav navbar-right">
						<li class="dropdown"><a class="dropdown dropdown-toggle"
							data-toggle="dropdown" href="#"> <i
								class="glyphicon glyphicon-user"></i><span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li role="presentation"><a href="index.jsp" role="menuitem">Profile</a></li>
								<!-- link to user profile -->
								<li role="presentation"><a href="logout.jsp"
									role="menuitem">Logout</a></li>
							</ul></li>
					</ul>
				</div>
			</nav>
			<!-- End Navigation bar -->

			<!-- Page Content -->

			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1>
							<%
								if (session.getAttribute("first_name") != null) {
							%>
							Welcome
							<%=session.getAttribute("first_name")%></h1>
						<%
							} else {
								response.sendRedirect("home.jsp");
							}
						%>


						<table class="profile_table">
							<tr>
								<td>Name:</td>
								<td><%=session.getAttribute("first_name")%>&nbsp;<%=session.getAttribute("last_name")%></td>
							</tr>
							<tr>
								<td>Email:</td>
								<td><%=session.getAttribute("email")%></td>
							</tr>
						</table>

					</div>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

</body>
</html>
