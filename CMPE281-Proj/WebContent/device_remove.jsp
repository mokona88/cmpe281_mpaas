<?xml version="1.0" encoding="UTF-8"?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%!
    String remove_query = "";
	String view_query = "";
	String update_query = "";
    ResultSet rs = null;
%>

<%
	String driver = "com.mysql.jdbc.Driver";
	String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
	String user = "cmpe281";
	String password = "group8";
	
	String uId = request.getParameter("userid");
	String iId = request.getParameter("instanceid");
	System.out.println('\n' + uId + '\n' + iId);
	remove_query = "delete from TestCase where userId=" + uId + " and instanceId=" + iId;
    update_query = "update EC2Instance Set available = true where instanceId = " + iId;
    
    try
    {
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, user, password);
        PreparedStatement statement = conn.prepareStatement(remove_query);
        statement.executeUpdate();
        statement.close();
        statement = conn.prepareStatement(update_query);
        statement.executeUpdate();
        statement.close();
        
        view_query = "SELECT userId, TestCase.instanceId as instanceId, public_ip  FROM TestCase, EC2Instance WHERE userId = " + uId + " AND TestCase.instanceId = EC2Instance.instanceId";
        statement = conn.prepareStatement(view_query);
        rs = statement.executeQuery();
        while (rs.next())
        {
            String ip = rs.getString("public_ip");
%>
                                <div class="panel panel-body">
                                    <!-- Add all needed code to this part -->
                                    <div class="col-xs-12 panel panel-default">
                                        <div class="col-xs-2">
                                            <br /> <a href="#"><img src="images/android_device.png" width="50" height="50" ></a>
                                        </div>
                                        
                                        <div class="col-xs-3 col-xs-offset-2">
                                            DeviceIP: <%= rs.getString("public_ip") %> <br />
                                            DevicePort: 5901
                                        </div>

                                        <div class="col-xs-2">
                                            <br/><br/>
                                            <div id="device-state">
                                                <img src="images/device-online.png" width="45" height="45"> <br/>
                                                <span class="form-group">Running</span>
                                            </div>
                                            <br/><br/>
                                        </div>
                                        <div class="col-xs-2">
                                            <button class="btn btn-primary" onclick="terminateConnection(userid, <%= rs.getInt("instanceId") %>)">Dispose</button>                                        
                                        </div>
                                    </div>

                                </div>

<%          
        }
        }catch (Exception e)
    {
        e.printStackTrace();
    }
%>