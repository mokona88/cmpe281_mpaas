<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.DecimalFormat"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Billing</title>
<link href="style_1.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="bootstrap3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="bootstrap3.3.4/css/bootstrap-theme.min.css">
<script type="text/javascript" rel="stylesheet"
	src="js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" rel="stylesheet"
	src="bootstrap3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
	<%!Integer diff;
	String platform;
	Integer total_min = 0;%>
	<div id="wrapper">

		<!-- Sidebar -->
		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<li class="sidebar-brand"><a href="index.jsp">Profile</a></li>
				<li><a href="services.jsp">Services</a></li>
				<li><a href="billing.jsp">Billing</a></li>
				<li><a href="report.jsp">Report</a></li>
			</ul>
		</div>
		<div id="page-content-wrapper">
			<!-- Navigation bar -->
			<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<a href="#" class="navbar-brand">CMPE281 - MOBILE PLATFORM
						TESTING - <%=session.getAttribute("first_name")%> <%=session.getAttribute("last_name")%></a>
				</div>

				<ul class="nav navbar-right">
					<li class="dropdown"><a class="dropdown dropdown-toggle"
						data-toggle="dropdown" href="#"> <i
							class="glyphicon glyphicon-user"></i><span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="index.jsp" role="menuitem">Profile</a></li>
							<!-- link to user profile -->
							<li role="presentation"><a href="logout.jsp" role="menuitem">Logout</a></li>
						</ul></li>
				</ul>
			</div>
			</nav>
			<!-- End Navigation bar -->

			<!-- Page Content -->
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1>Billing and Cost Model</h1>
						<div>
							<div>
								<table id="customers">
									<tr>
										<th>Device Type</th>
										<th>SDK</th>
										<th>Usage</th>
									</tr>
									<tr>
										<td>Android</td>
										<td>19</td>
										<td>$0.06/minute</td>
									</tr>
									<tr>
										<td>iPhone</td>
										<td>8</td>
										<td>$0.06/minute</td>
									</tr>
									<tr>
										<td>Windows</td>
										<td>4</td>
										<td>$0.05/minute</td>
									</tr>
								</table>
								<p>
								<h2>Estimate Billing</h2>
								<table id="customers">
									<tr>
										<th>Instance</th>
										<th>Start Time</th>
										<th>End Time</th>
										<th>Total Time</th>
										<th>Cost</th>
									</tr>

									<%
										Connection con = null;
										PreparedStatement ps = null;
										ResultSet rs = null;

										String driverName = "com.mysql.jdbc.Driver";
										String url = "jdbc:mysql://cmpe.c3d3ryp592fa.us-west-1.rds.amazonaws.com:3306/cmpe281";
										String user = "cmpe281";
										String dbpsw = "group8";

										String id = (session.getAttribute("id")).toString();
										System.out.println("user id is " + id);
										String sql = "select distinct instanceId,startTime,endTime, timestampdiff(minute,startTime,endTime) as diff from TestCase t1 Inner join users u1 on t1.userId=? order by instanceId";

										try {
											Class.forName(driverName);
											con = DriverManager.getConnection(url, user, dbpsw);
											ps = con.prepareStatement(sql);
											ps.setString(1, id);
											rs = ps.executeQuery();

											while (rs.next()) {
									%>
									<tr>
										<td><%=rs.getInt("instanceId")%></td>
										<td><%=rs.getTimestamp("startTime")%></td>
										<%if (rs.getTimestamp("endTime") == null) { %>
										<td>Still Running</td>
										<td>-------<%total_min=0; %></td>
										<td>$ .00<%diff=0;%>
										<%} 
										else {%>
										<td><%=rs.getTimestamp("endTime")%></td>
										<td><% int hr=rs.getInt("diff")/60;
											   int min=rs.getInt("diff") % 60;
											 out.print(hr);%> hrs <%out.print(min);%> mins
											   </td>
										<td>$ <%
											DecimalFormat dFormat = new DecimalFormat("#.00");
													diff = rs.getInt("diff");%>
													<!--if (platform == "iOS" || platform == "Android") {-->
										 <%=dFormat.format(diff * 0.06)%> <!--
 													} 
													else {
 										 dFormat.format(diff * 0.05)%>
												 	}
 												-->
										</td>
									</tr>
									<%
										total_min = total_min + diff;
									  }}
										} catch (SQLException sqe) {
											out.println(sqe);
										}
									%>
									<tr>
										<td>TOTAL</td>
										<td></td>
										<td></td>
										<td><%=total_min/60%> hrs <%=total_min%60%> mins </td>
										<td>$ <%
											DecimalFormat dFormat = new DecimalFormat("#.00");
										%>
																			
 	 <!--if (platform == "iOS" || platform == "Android") { -->
 <%=dFormat.format(total_min * 0.06)%> 
 <!--} else {%> dFormat.format(total_min * 0.05)}-->
										</td>
									</tr>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->
	</div>
	<!-- /#wrapper -->
</body>
</html>